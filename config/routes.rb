Rails.application.routes.draw do
  resources :posts
  get 'debug' => 'debug#index'

  root 'posts#index'
end
